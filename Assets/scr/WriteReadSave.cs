﻿using Newtonsoft.Json;
using System.IO;
using UnityEngine;
using Zenject;

public class WriteReadSave {

	private static string _filename = "data.sv";
    private string _path = Path.Combine(Application.persistentDataPath, _filename);

    [Inject]
    private State _state;

    [Inject]
    private UserData _userdata;

    public void WriteSave() {
/*
        string dataAsJson = JsonConvert.SerializeObject(_userdata);
        File.WriteAllText (_filename, dataAsJson);
        Debug.Log("Save Newtonsoft JSON: " + dataAsJson + "\nGame Saved");
*/
        string dataAsJson = JsonUtility.ToJson(_userdata);
        File.WriteAllText(_path, dataAsJson);
        //Debug.Log("Save Utility JSON: " + dataAsJson);
        Debug.Log("Game Saved\nPath: " + _path);
    }

    public void ReadSave() {
/*
        if (File.Exists(_filename)) {
            string dataAsJson = File.ReadAllText(_filename);
            Debug.Log("Data: " + dataAsJson);

            UserData ud = JsonConvert.DeserializeObject<UserData>(dataAsJson);
            //_userdata = JsonConvert.DeserializeObject<UserData>(dataAsJson);//?

            Debug.Log("Newtonsoft coins: " + _userdata.Coins + " Newtonsoft h: " + _userdata.MaxHeight);
            Debug.Log("ud coins: " + ud.Coins + " ud h: " + ud.MaxHeight);

            _userdata.Coins = ud.Coins;
            _userdata.MaxHeight = ud.MaxHeight;
        }
*/
        if (File.Exists(_path)) {

            string dataAsJson = File.ReadAllText(_path);

            Debug.Log("Data: " + dataAsJson);
            UserData ud = JsonUtility.FromJson<UserData>(dataAsJson);
            //_userdata = JsonUtility.FromJson<UserData>(dataAsJson);//?

            //Debug.Log("Utility coins: " + _userdata.Coins + " Utility h: " + _userdata.MaxHeight);
            Debug.Log("ud coins: " + ud.Coins + " ud h: " + ud.MaxHeight);

            _userdata.Coins = ud.Coins;
            _userdata.MaxHeight = ud.MaxHeight;
        }
    }
}
