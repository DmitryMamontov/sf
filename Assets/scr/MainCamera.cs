﻿using UnityEngine;
using DG.Tweening;
using Zenject;
public class MainCamera : MonoBehaviour {

    public GameObject Scale;
    private Vector3 _defaultCamPos;
    private float _corrector;

    [Inject]
	private State _state;
    [Inject] private PlayerDeadSignal _playedDeadSignal;
    //[Inject]
    //private Config _config;
    private void Start(){
        
        //wtf...

        tk2dCamera camera = GetComponent<tk2dCamera>();
        Vector2 sprite_size = Scale.GetComponent<SpriteRenderer>().sprite.rect.size;

        float camSize = camera.forceResolution.y;
         _corrector = camSize * 0.013f;

        float targetScaleSize = (camSize - sprite_size.y) * 0.012f;

        // if(camera.forceResolution == new Vector2(1080, 1920)){
        //     _corrector = camSize * 0.0023f;
        //     targetScaleSize = (camSize - sprite_size.y) * 0.001f;
        // }

        //int height = Screen.height;
        //_corrector = height * 0.014f;
        
        // float camSize = camera.ScreenResolution.y;
        // _corrector = camSize * 0.5f;

        // float camSize = camera.TargetResolution.y;
        // Debug.Log("camSize: " + camSize);
        // _corrector = camSize * 0.01f;

        // float camSize = camera.nativeResolutionHeight;
        // Debug.Log("camSize: " + camSize);
        // _corrector = camSize * 0.01f;

        // Vector3 v3 = Scale.GetComponent<SpriteRenderer>().bounds.size;
		// Debug.Log("Ybs: " + v3.y + " X: " + v3.x);
		// float v1 = Scale.GetComponent<SpriteRenderer>().sprite.pixelsPerUnit;
		// Debug.Log("Yppy: " + v1);
		
		//Debug.Log("Yrs: " + sprite_size.y);
		// float h = Scale.GetComponent<SpriteRenderer>().sprite.rect.height;
		// Debug.Log("Yh: " + h);

		//float targetSize = (height - sprite_size.y);
        
		Vector3 current = Scale.transform.localScale;
		Scale.transform.localScale = new Vector3(current.x, current.y + targetScaleSize, current.z);

         _defaultCamPos = new Vector3(0, _corrector, -10);
        transform.position = _defaultCamPos;

        _playedDeadSignal.Listen(resetCamera);

    }

    private void resetCamera(){
        transform.position = _defaultCamPos;
    }

    private void Update () {

		if (_state.isPlay & _state.playerPos >= _corrector){
            transform.position = new Vector3(0, _state.playerPos, -1);
        }
	}

    private void OnApplicationQuit(){
        _playedDeadSignal.Unlisten(resetCamera);
    }
}
