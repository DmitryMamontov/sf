﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using Zenject;

public class Player : MonoBehaviour {
	public float currentlyHeight;
	public tk2dBaseSprite PlayerSprites;

    [Inject] private Config _cfg;

	[Inject] private State _state;

    [Inject] private UserData _userdata;

	[Inject] private PlayerDeadSignal _playedDeadSignal;

	[Inject] private StateSystem _stateSystem;

	[Inject] private GreenZone _greenZone;

	private void Start(){
		PlayerSprites = GetComponent<tk2dBaseSprite>();
		_playedDeadSignal.Listen(resetState);
	}

	private void resetState(){
		_state.isPlay = false;
		transform.DOKill();
		transform.position = new Vector2(0, 0);
	}

	private void Update () {
		
		currentlyHeight = transform.position.y;
		
		if(!_state.isFirstTap & _state.isPlay) {

            if (currentlyHeight >= _cfg.BaseStr){
                if (currentlyHeight > _userdata.MaxHeight) { _userdata.MaxHeight = currentlyHeight; }
                transform.DOMoveY(0, _cfg.DownSpeed);
				_state.isUp = false;
			}

			if(currentlyHeight - 0.01f <= 0){
				transform.DOMoveY(_cfg.BaseStr, _cfg.UpSpeed);
				_state.isUp = true;
			}
		} 
		//else{transform.position = new Vector2(0, 0);}
		_state.playerPos = currentlyHeight;
		setSprite();
	}

    private void OnCollisionEnter2D(Collision2D collision) {
        var collisionGameObject = collision.gameObject;
        if (collisionGameObject.CompareTag(Tags.Coin)) {
			//if(collisionGameObject != null) uncomment if we have one more access points to coin obj
			Destroy(collisionGameObject);
            _userdata.Coins += 1;
        }
    }
	private void setSprite(){
		if(currentlyHeight >= _cfg.BaseStr){
			_stateSystem.Active(StateSystem.PlayerState.TriangleUpend);
		}

		if(currentlyHeight - 0.01f <= 0){
			_stateSystem.Active(StateSystem.PlayerState.Rectangle);
		}

		if(_state.isFirstTap || !_state.isPlay){
			_stateSystem.Active(StateSystem.PlayerState.Cube);
		}

		if(_greenZone.PlayerOnMiddle){
			if(_state.isUp){
                _stateSystem.Active(StateSystem.PlayerState.Triangle);
            } else {
                _stateSystem.Active(StateSystem.PlayerState.Cube);
            }
		}
	}
}
