﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Zenject;

public class CoinGenerator : MonoBehaviour {
	public GameObject coinPrefab;

	private float _currentCoinPos;
	private int _multForNextPos = 1;

	[Inject]
	private State _state;
    [Inject]
    private Config _config;

	private void Start () {
        _config.resetCoinConfig();

        coinPrefab.transform.position = new Vector3(0, _config.DistBetweenCoin, 0);
		_currentCoinPos = _config.DistBetweenCoin;
		Instantiate(coinPrefab, new Vector3(0, _currentCoinPos, 0), Quaternion.identity);
	}

    private void Update() {

        if (canSpawn()) {
            coinSpawn();
        }
		
	}

    private void coinSpawn(){

        _currentCoinPos = _currentCoinPos + _config.DistBetweenCoin;

        if (_currentCoinPos >= (_config.DecreaseDistEvery * _multForNextPos)){
            _config.DistBetweenCoin -= _config.DecreaseDistBy;
            if (_config.DistBetweenCoin < 0.1){
                _config.DistBetweenCoin = 0.1f;
            }
            _multForNextPos++;
        }
        
        Instantiate(coinPrefab, new Vector3(0, _currentCoinPos, 0), Quaternion.identity);
    }

    private bool canSpawn(){
        return _state.playerPos > _currentCoinPos - _config.DistanceCoinGenerate;
        //return _currentCoinPos < maxGenerateDistance; //static spawn

    }
}
