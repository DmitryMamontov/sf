﻿using UnityEngine;

public class Config {

    //person config
    private float _upSpeed;
    private float _downSpeed;
    //private float _nextJumpAddStr;
    //private float _redZoneDegree;
    private float _baseStr;

    public float UpSpeed {
        get { return _upSpeed; }
        set {
            if (value > 0.11f) {
                _upSpeed = value;
            }
        }
        //set { _upSpeed = Mathf.Clamp(value, 0.11f, float.MaxValue); } //lost previous value
    }

    public float DownSpeed {
        get { return _downSpeed; }
        set {
            if (value > 0.11f) {
                _downSpeed = value;
            }
        }
    }

    public float NextJumpAddStr;
    public float ClickZoneDegree;

    public float BaseStr {
        get { return _baseStr; }
        set {
            if (value > 0f) {
                _baseStr = value;
            }
        }
    }


    //coin config

    private float _distBetweenCoin;

    public float DistBetweenCoin{
        get { return _distBetweenCoin; }
        set {
            if (value > 0f) {
                _distBetweenCoin = value;
            }
        }
    }

    public float DecreaseDistEvery;
    public float DecreaseDistBy;
    public float DistanceCoinGenerate = 50f;

    public void resetConfig(){
        resetPersonConfig();
        resetCoinConfig();
    }

    public void resetCoinConfig(){

        DistBetweenCoin = 12f;
        DecreaseDistEvery = 25f;
        DecreaseDistBy = 0.5f;
        DistanceCoinGenerate = 50f;
    }

    public void resetPersonConfig(){
        UpSpeed = 2f;
        DownSpeed = 2f;
        NextJumpAddStr = 2f;
        ClickZoneDegree = 0.01f;
        BaseStr = 12f;
    }
}
