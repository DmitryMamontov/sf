﻿using System;
using UnityEngine;
using Zenject;

public class StateSystem : MonoBehaviour {

	// [Inject] private Player _player;
	private PlayerState _currentState;
	public tk2dBaseSprite _playerSprites;

	private void Start(){
		//_playerSprites = _player.PlayerSprites.GetComponent<tk2dBaseSprite>();
	}

	public void Active(PlayerState st){
			switch (st)
			{
				case PlayerState.Cube:
					//Debug.Log("Case 1");

					_currentState = PlayerState.Cube;
					_playerSprites.SetSprite("Cube");
					break;

				case PlayerState.Rectangle:
					//Debug.Log("Case 2");

					_currentState = PlayerState.Rectangle;
					_playerSprites.SetSprite("Rectangle");
					break;

				case PlayerState.Triangle:
					//Debug.Log("Case 3");

					_currentState = PlayerState.Triangle;
					_playerSprites.SetSprite("Triangle");
					break;

				case PlayerState.TriangleUpend:
					//Debug.Log("Case 4");

					_currentState = PlayerState.TriangleUpend;
					_playerSprites.SetSprite("TriangleUpend");
					break;

				case PlayerState.None:
					//Debug.Log("Case 5");

					DeActive();
					break;

				default:
					DeActive();
					//Debug.Log("Default case");
					break;
			}
	}
	public void DeActive(){
		_currentState = PlayerState.None;
	}

	// public StateP getProperty(){
	// 	return StateP.None;
	// }

	public PlayerState getCurrentState(){
		return _currentState;
	}

	[Flags]
	public enum PlayerState : byte
	{
		None = 0,
		Cube = 1, // 1 << 0
		Triangle = 2, // 1 << 1
		TriangleUpend = 4, // 1 << 2
		Rectangle = 8 // 1 << 3
	}
}
