﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class UserData {

	public float MaxHeight;
    public int Coins;
	
}
