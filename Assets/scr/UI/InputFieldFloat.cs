﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputFieldFloat : MonoBehaviour {
	public tk2dUITextInput TextInput;
	public float valueFloat = 0.1f;

	private void Update(){
		float result;
		float.TryParse(TextInput.Text, out result);
		if(result > 0f){
			valueFloat = result;
		}
		TextInput.Text = valueFloat.ToString();
	}
}
