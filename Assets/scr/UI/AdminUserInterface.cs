﻿using UnityEngine;
using Zenject;
using NumericTextInput;

public class AdminUserInterface : MonoBehaviour {

    public tk2dUITextInput UpSpeed;
    public tk2dUITextInput DownSpeed;
    public tk2dUITextInput NextJumpAddStr;
    public tk2dUITextInput ClickZoneDegree;
    public tk2dUITextInput BaseStr;

    public tk2dUITextInput DistBetweenCoin;
    public tk2dUITextInput DecreaseDistEvery;
    public tk2dUITextInput DecreaseDistBy;
    
    private bool _isShow = false;

    [Inject]
    private Config _cfg;
    [Inject]
    private State _state;
    [Inject] private Controller _controller;

    private InputFieldFloat _UpSpeedFloat;
    private InputFieldFloat _DownSpeedFloat;
    private InputFieldFloat _NextJumpAddStrFloat;
    private InputFieldFloat _BaseStrFloat;
    private void Start () {
        _controller.HideAdminPanel();
        //hideUI();
        resetSettings();
        _UpSpeedFloat = UpSpeed.GetComponent<InputFieldFloat>();
        _DownSpeedFloat = DownSpeed.GetComponent<InputFieldFloat>();
        _NextJumpAddStrFloat = NextJumpAddStr.GetComponent<InputFieldFloat>();
        _BaseStrFloat = BaseStr.GetComponent<InputFieldFloat>();
    }

    public void resetSettings() {
        _cfg.resetConfig();
        configToUi();
    }

    public void updateConfig() {
        UiToConfig();
        configToUi();
    }

    // private void UiToConfig() {
    //     float parseResult;
    //     float.TryParse(UpSpeed.Text, out parseResult);
    //     _cfg.UpSpeed = parseResult;
    //     float.TryParse(DownSpeed.Text, out parseResult);
    //     _cfg.DownSpeed = parseResult;
    //     float.TryParse(NextJumpAddStr.Text, out _cfg.NextJumpAddStr);
    //     float.TryParse(RedZoneDegree.Text, out _cfg.ClickZoneDegree);
    //     float.TryParse(BaseStr.Text, out parseResult);
    //     _cfg.BaseStr = parseResult;
    // }

    private void UiToConfig() {

        _cfg.UpSpeed = UpSpeed.getFloatValueMoreZero();
        _cfg.DownSpeed = DownSpeed.getFloatValueMoreZero();
        _cfg.NextJumpAddStr = NextJumpAddStr.getFloatValueMoreZero();
        _cfg.ClickZoneDegree = ClickZoneDegree.getFloatValueMoreZero();
        _cfg.BaseStr = BaseStr.getFloatValueMoreZero();

        _cfg.DistBetweenCoin = DistBetweenCoin.getFloatValueMoreZero();
        _cfg.DecreaseDistEvery = DecreaseDistEvery.getFloatValueMoreZero();
        _cfg.DecreaseDistBy = DecreaseDistBy.getFloatValueMoreZero();

    }

    private void configToUi() {
        UpSpeed.Text = _cfg.UpSpeed.ToString();
        DownSpeed.Text = _cfg.DownSpeed.ToString();
        NextJumpAddStr.Text = _cfg.NextJumpAddStr.ToString();
        ClickZoneDegree.Text = _cfg.ClickZoneDegree.ToString();
        BaseStr.Text = _cfg.BaseStr.ToString();

        DistBetweenCoin.Text = _cfg.DistBetweenCoin.ToString();
        DecreaseDistEvery.Text = _cfg.DecreaseDistEvery.ToString();
        DecreaseDistBy.Text = _cfg.DecreaseDistBy.ToString();
    }

    public void showHideAdminUI() {
        if (_isShow) {
            _isShow = false;
            _controller.HideAdminPanel();
            //hideUI();
        } else {
            _isShow = true;
            _controller.ShowAdminPanel();
            //configToUi();//if we had any changes in config
            //showUI();
        }
    }

    //wrong place
    // private void UpgradeTrampo() {

    //     float parseResult;

    //     float.TryParse(UpSpeed.Text, out parseResult);
    //     UpSpeed.Text = (parseResult - 0.01f).ToString();

    //     float.TryParse(DownSpeed.Text, out parseResult);
    //     DownSpeed.Text = (parseResult + 0.015f).ToString();

    //     float.TryParse(NextJumpAddStr.Text, out _cfg.NextJumpAddStr);
    //     NextJumpAddStr.Text = (parseResult + 0.01f).ToString();

    //     float.TryParse(BaseStr.Text, out parseResult);
    //     BaseStr.Text = (parseResult + 3f).ToString();

    //     //_cfg.UpSpeed -= 0.01f;
    //     //_cfg.DownSpeed += 0.15f;
    //     //_cfg.NextJumpAddStr += 0.1f;
    //     //_cfg.BaseStr += 3f;
    // }

    private void UpgradeTrampo() {

        UpSpeed.Text = (_UpSpeedFloat.valueFloat - 0.01f).ToString();

        DownSpeed.Text = (_DownSpeedFloat.valueFloat + 0.015f).ToString();

        NextJumpAddStr.Text = (_NextJumpAddStrFloat.valueFloat + 0.01f).ToString();

        BaseStr.Text = (_BaseStrFloat.valueFloat + 3f).ToString();

    }

    //wrong place
    private void startFT() {
        UiToConfig();
        _state.isFirstTap = true;
        _state.isPlay = true;
    }
}
