﻿using UnityEngine;
using DG.Tweening;
using Zenject;

public class GreenZone : MonoBehaviour {

    [Inject] private State _state;
    [Inject] private Config _cfg;
    [Inject] private PlayerDeadSignal _playedDeadSignal;

    public bool PlayerOnMiddle;

    private bool inputFromUser(){
        #if UNITY_EDITOR || UNITY_STANDALONE
            //Debug.Log("UNITY_EDITOR_WIN");
            return Input.GetMouseButton(0);
        #else
        //#if UNITY_ANDROID || UNITY_IOS
            //Debug.Log("ANDROID or IOS");
            return Input.touches.Length > 0;
        #endif
    }

    private void Start () {
        transform.localPosition = new Vector2(0, -1.275f);
    }
	
	private void Update () {

        var _resultPos = _state.playerPos;
        _resultPos = (_cfg.BaseStr / _resultPos);
        transform.localPosition = new Vector2(0, -1.275f + (2.55f / _resultPos));

        if (_state.isFirstTap){
			if (inputFromUser()){
				//_state.isLife = true;
                _state.isFirstTap = false;
                _state.isClick = true;
			}
		}
    }

    private void OnCollisionStay2D(Collision2D collision) {
        var collisionGameObject = collision.gameObject;

        if(inputFromUser()){
            if (collisionGameObject.CompareTag(Tags.MiddleClickZone)) {
                PlayerOnMiddle = true;
                //_state.currentState = State.PlayerFlag.None;
                // if(_state.isUp){
                //     _state.currentState = State.PlayerFlag.Triangle;
                // } else {
                //     _state.currentState = State.PlayerFlag.Cube;
                // }
            }
            _state.isClick = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision) {
        var collisionGameObject = collision.gameObject;

        if (!_state.isClick) {
                //_state.isLife = false;
                _playedDeadSignal.Fire();
        }
        _state.isClick = false;
        PlayerOnMiddle = false;
    }

    // private bool isClickZone(GameObject collisionGameObject){
    //     //this is really need?
    //     return collisionGameObject.CompareTag(Tags.MiddleClickZone) | collisionGameObject.CompareTag(Tags.TopClickZone) | collisionGameObject.CompareTag(Tags.BottomClickZone);
    // }
}
