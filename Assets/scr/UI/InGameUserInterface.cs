﻿using UnityEngine;
using DG.Tweening;
using Zenject;
using TMPro;

public class InGameUserInterface : MonoBehaviour {
    public TextMeshPro displayHeightText;
    public TextMeshPro maxHeightText;
    public TextMeshPro coins;

	[Inject]
	private State _state;

    [Inject]
    private UserData _userdata;

    [Inject]
    private WriteReadSave _save;

    private void Start () {
        _save.ReadSave();
    }
	
	private void Update () {
        coins.text = string.Format("Coin: {0}", _userdata.Coins);
        maxHeightText.text = string.Format("Max: {0:F1}", _userdata.MaxHeight);
        if (_state.playerPos > 0) { displayHeightText.text = string.Format("{0:F1}", _state.playerPos); } else { displayHeightText.text = "0.0"; }

    }

    private void OnApplicationQuit() {
        _save.WriteSave();
    }
}
