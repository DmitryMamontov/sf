﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class MainMenuUserInterface : MonoBehaviour {

    [Inject] private State _state;

    [Inject] private Controller _controller;
    
	private void Update () {
        if(_state.isPlay){
            _controller.HideMainMenu();
        } else {
            _controller.ShowMainMenu();
        }

    }
    /*
    private void UpgradeTrampo() {
        _cfg.UpSpeed -= 0.01f;
        _cfg.DownSpeed += 0.15f;
        _cfg.NextJumpAddStr += 0.1f;
        _cfg.BaseStr += 3f;
    }

    private void startFT() {
        //_cfg.resetConfig();
        _state.isFirstTap = true;
    }
    */
}
