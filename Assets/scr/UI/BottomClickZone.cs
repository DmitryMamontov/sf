﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Zenject;

public class BottomClickZone : MonoBehaviour {

    [Inject] private State _state;
    [Inject] private Config _cfg;
    [Inject] private PlayerDeadSignal _playedDeadSignal;

    private void Start () {
        _playedDeadSignal.Listen(resetScale);
	}
    private void resetScale(){
        transform.localScale = new Vector3(1, 0.13f, 1);
		transform.localPosition = new Vector2(0, -1.12f);
    }

    private void OnCollisionExit2D(Collision2D collision) {
        var collisionGameObject = collision.gameObject;
        if (collisionGameObject.CompareTag(Tags.GreenZone)) {

            transform.localScale = new Vector3(1, transform.localScale.y - _cfg.ClickZoneDegree, 1);
            transform.localPosition = new Vector3(0, transform.localPosition.y - (_cfg.ClickZoneDegree * 1.6f), 1);
        }
    }

    private void OnApplicationQuit(){
        _playedDeadSignal.Unlisten(resetScale);
    }
}
