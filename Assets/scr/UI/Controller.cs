﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//UIController
public class Controller : MonoBehaviour {

	[SerializeField] public GameObject AdminUI;
	[SerializeField] public GameObject MainMenuUI;

	public void HideMainMenu(){
		MainMenuUI.transform.localPosition = new Vector3(0, -20, 1);
	}

	public void ShowMainMenu(){
		MainMenuUI.transform.localPosition = new Vector3(0, 0, 1);
	}

	public void HideAdminPanel(){
		AdminUI.transform.localPosition = new Vector3(10, 0, 1);
	}

	public void ShowAdminPanel(){
		AdminUI.transform.localPosition = new Vector3(0, 0, 1);
	}
}
