﻿public static class Tags {

	public const string Player = "Player";
	public const string TopClickZone = "TopClickZone";
    public const string MiddleClickZone = "MiddleClickZone";
    public const string BottomClickZone = "BottomClickZone";
    public const string GreenZone = "GreenZone";
    public const string Coin = "Coin";

    //string[] str = UnityEditorInternal.InternalEditorUtility.tags;
}
