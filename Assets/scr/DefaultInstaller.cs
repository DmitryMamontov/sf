using UnityEngine;
using Zenject;

public class DefaultInstaller : MonoInstaller<DefaultInstaller>
{
    public override void InstallBindings()
    {
        Container.Bind<Config>().AsSingle();
        Container.Bind<State>().AsSingle();
        Container.Bind<WriteReadSave>().AsSingle();
        Container.Bind<UserData>().AsSingle();

        Container.DeclareSignal<PlayerDeadSignal>();
        //Container.BindSignal<PlayerDeadSignal>();

    }
}