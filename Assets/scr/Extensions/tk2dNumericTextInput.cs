﻿namespace NumericTextInput
{
    public static class tk2dNumericTextInput
    {
        private static float _value = 0.001f;
        public static float getFloatValue(this tk2dUITextInput tkTextInput){
            float result;
            float.TryParse(tkTextInput.Text, out result);
            return result;
        }

        public static float getFloatValueMoreZero(this tk2dUITextInput tkTextInput){
            float result = tkTextInput.getFloatValue();
            if(result > 0f){
			    _value = result;
		    }
            return _value;
        }
    }
}
