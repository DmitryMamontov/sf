﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Zenject;
public class Trampoline : MonoBehaviour {

	private tk2dBaseSprite _trampolineSprites;

	[Inject]
	private State _state;

	private void Start () {
		_trampolineSprites = GetComponent<tk2dBaseSprite>();
	}
	
	private void Update () {
		if( _state.playerPos < 1.5f && _state.isUp){
			swithToDeformPlatform();
		} else {
			swithToPlatform();
		}
	}

	public void swithToPlatform(){
		_trampolineSprites.SetSprite("platform");
	}
	public void swithToDeformPlatform(){
		_trampolineSprites.SetSprite("DeformPlatform");
	}
}
